import React, { useState, useEffect } from 'react';
import { getSession, socketEvento,obtenerUsuarios } from '../services/getSession';
import { Modal, Button, Alert } from "react-bootstrap"

function Prueba() {

    let [posicion, setPosicion] = useState([]);
    let [alerta, setAlerta] = useState([]);
    let [dispositivos, setDispositivos] = useState([]);
    let [datos, setDatos] = useState([])
    useEffect(() => {
        getSession()
            .then((result) => {
                console.log(result);
                obtenerUsuarios().then(e => console.log(e)).catch(e=>console.log(e));
                socketEvento(
                    (evento) => {
                        let datos = JSON.parse(evento.data);
                     //   console.log(datos);
                        if (datos.devices) {
                            setDispositivos((datos.devices))
                        } else if (datos.positions) {
                            setPosicion((datos.positions))
                        } else if (datos.events) {
                            setAlerta((datos.events))
                        }
                    }

                );

            })
            .catch(e => {
                console.log(e);
            })
    }, [])

    return <>
        <h4> {(JSON.stringify(dispositivos))} </h4>
        <br />
        <h4>{JSON.stringify(alerta)}</h4>
        <br />
        <h4>{JSON.stringify(posicion)}</h4>
        <ModalComponent setEvento={setAlerta} evento={(alerta && alerta.length > 0) ? (alerta[0]) : null} />
    </>


}

function ModalComponent({ evento,setEvento }) {
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const clg = () => console.log(evento); 
    return (
        <>
                {clg()}

        ( {evento ?

            (
            <Modal show={evento} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>ALARMA!!!!!!</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Alert show={evento} variant="danger">


                        <Alert.Heading> dispositivo: {evento.deviceId} </Alert.Heading>
                    <i class="bi bi-exclamation-triangle"></i>
                        <p>
                            <h1>{evento.attributes.alarm}</h1>
                        </p>
                        <hr />
                        <div className="d-flex justify-content-end">
                        </div>
                    </Alert>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={() => setEvento(null)}>
                        Close
            </Button>
                    <Button variant="primary" onClick={() => setEvento(null)}>
                        Save Changes
            </Button>
                </Modal.Footer>
            </Modal>) : null})
        </>
    );
}




export default Prueba;