
//import io  from "socket.io-client";


const header = {
    'Authorization': 'Basic ' + Buffer.from('admin' + ":" + 'admin').toString('base64'),
    'Content-Type': 'application/x-www-form-urlencoded'
};
const url = "http://localhost:8082/api/session";
const urlUser = 'http://localhost:8082/api/users'

const obtenerUsuarios = async () => {
    try {
        const response = await fetch(urlUser, {
            method: 'GET', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Authorization': 'Basic ' + Buffer.from('admin' + ":" + 'admin').toString('base64'),
                'Content-Type': 'application/json'
            },
            credentials: 'same-origin', // include, *same-origin, omit)
        })
        if (response.status >= 200 || response.status <= 299) {
            return await response.text();
        } else {
            throw new Error({ msg: response.statusText })
        }
    } catch (e) {
        console.log(e);
        throw e;
    }
}

const getSession = async () => {
    try {
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                'Authorization': 'Basic ' + Buffer.from('admin' + ":" + 'admin').toString('base64'),
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: new URLSearchParams({
                'email': 'admin',
                'password': 'admin'
            }) // body data type must match "Content-Type" header
        });
        if (response.status >= 200 || response.status <= 299) {
            return await response.json();
        } else {
            throw new Error({ msg: response.statusText })
        }
    } catch (e) {
        console.log(e);
        throw e;
    }

}
/**
 * 
 * @param {Function} onMessage 
 */
const socketEvento = (onMessage) => {
    let webSocket = new WebSocket("ws://localhost:8082/api/socket");

    /*webSocket.onopen = (event => {
        console.log(event);
    })*/

    webSocket.onmessage = onMessage;

}

export { getSession, socketEvento, obtenerUsuarios};